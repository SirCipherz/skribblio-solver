#!/usr/bin/python3
import re

with open('./french') as raw:
    guess = [raw.read().split('\n')]

while len(guess[-1]) > 1:
    guess.append([])

    word  = input('Mot : ')
    regex = word.replace('-', "[a-z]")
    for i in guess[-2]:
        if re.search(regex, i) != None:
            guess[-1].append(i)

    print('\n'.join(guess[-1]))
